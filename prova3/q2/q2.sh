#!/bin/bash

d=$1
o=$2

if [ ! -d "$d" ]; then
  echo "O diretório $d não existe."
  exit 1
fi

case $o in
    -a) 
        echo "Diretórios em $d:"
        for i in "$d"/*; do
            if [ -d "$i" ]; then
                echo "$i"
            fi
        done
    ;;
    -b) 
        echo "Executáveis em $d:"
        for i in "$d"/*; do
            if [ -f "$i" ] && [ -x "$i" ]; then
                echo "$i"
            fi
        done
    ;;
    -c) 
        for i in "$d"/*; do
            if [ -f "$i" ] && [ "${i##*.}" = "sh" ]; then
                echo "$i"
            fi
        done
    ;;
    *)
        echo "Opção inválida. Use -a para diretórios, -b para executáveis, -c para scripts shell."
    ;;
esac
