#!/bin/bash

f=$1
o=$2

if [ ! -f "$f" ]; then
  echo "O arquivo $f não existe."
  exit 1
fi

case $o in
    -r) 
        echo "Removendo linhas em branco de $f:"
        grep -v '^$' "$f" > temp && mv temp "$f"
    ;;
    -c)
        count=$(grep -c '^$' "$f")
        echo "Número de linhas em branco: $count"
    ;;
    *) # Invalida
        echo "Opção inválida. Use -r para remover linhas em branco, -c para contar linhas em branco."
    ;;
esac