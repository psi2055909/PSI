#!/bin/bash

p=$1

has_upper=0
has_lower=0
has_digit=0

while IFS= read -r -n1 c; do
    if [[ "$c" =~ [A-Z] ]]; then
        has_upper=1
    elif [[ "$c" =~ [a-z] ]]; then
        has_lower=1
    elif [[ "$c" =~ [0-9] ]]; then
        has_digit=1
    fi
done <<< "$p"

if [ "$has_upper" -eq 1 ] && [ "$has_lower" -eq 1 ] && [ "$has_digit" -eq 1 ]; then
    echo "Senha válida"
else
    echo "Senha inválida"
fi



