#!/bin/bash

arquivo=$1

if [ ! -f "$arquivo" ]; then
  echo "Arquivo não encontrado."
  exit 1
fi

while IFS= read -r linha; do
  for palavra in $linha; do
    if [[ $palavra =~ ^R\$[0-9]+\.[0-9]{3},[0-9]{2}$ || $palavra =~ ^R\$[0-9]+,[0-9]{2}$ ]]; then
      echo "$palavra"
    fi
  done
done < "$arquivo"
