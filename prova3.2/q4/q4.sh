#!/bin/bash

f="/etc/group"

while IFS= read -r lne; do
  cnt=0
  pat=""

  while [[ "$lne" == *":"* ]]; do
    cmp=${lne%%:*}
    lne=${lne#*:}
    pat="$cmp:$pat"
    cnt=$((cnt + 1))
  done

  pat="${lne}:$pat"

  echo "$pat"
done < "$f"
