#!/bin/bash

RND=$((RANDOM % 100 + 1))
link=$(curl -s "https://dummyjson.com/user/$RND")

nme=$(echo "$link" | grep -oP '"firstName":"\K[^"]+')
sbe=$(echo "$link" | grep -oP '"lastName":"\K[^"]+')
usr=$(echo "$link" | grep -oP '"username":"\K[^"]+')
pwd=$(echo "$link" | grep -oP '"password":"\K[^"]+')
ip=$(echo "$link" | grep -oP '"ip":"\K[^"]+')
eml=$(echo "$link" | grep -oP '"email":"\K[^"]+')

echo "$nme $sbe::$usr::$pwd::$ip::$eml"