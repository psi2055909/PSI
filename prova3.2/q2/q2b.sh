#!/bin/bash

abc="access.log"

if [ -f "$abc" ]; then
  grep -oP '^\d+\.\d+\.\d+\.\d+' "$abc" | sort | uniq
fi