#!/bin/bash

FL="access.log"

if [ -f "$FL" ]; then
  grep -oP '^\d+\.\d+\.\d+\.\d+' "$FL" | sort | uniq > ips.tmp
  CNT=$(wc -l < ips.tmp)
  if [ "$CNT" -gt 0 ]; then
    RND=$((RANDOM % CNT + 1))
    RND_IP=$(sed -n "${RND}p" ips.tmp)
    echo "$RND_IP"
  else
    echo "Nenhum IP encontrado."
  fi
  rm ips.tmp
fi