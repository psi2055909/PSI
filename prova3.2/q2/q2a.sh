#!/bin/bash

FL="access.log"
URL="https://raw.githubusercontent.com/linuxacademy/content-elastic-log-samples/master/access.log"

if [ -f "$FL" ]; then
  echo "O arquivo '$FL' já existe."
else
  echo "O arquivo '$FL' não existe. Baixando..."
  curl -O "$URL"
  if [ -f "$FL" ]; then
    echo "O arquivo '$FL' foi baixado com sucesso."
  else
    echo "Falha ao baixar o arquivo '$FL'."
  fi
fi