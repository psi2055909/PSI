#!/bin/bash

inp=$1       
cnt=1        
tmp=$(mktemp)

while IFS= read -r line; do
  if [ "$line" = "----" ]; then
    if [ -s "$tmp" ]; then
      mv "$tmp" "part_$cnt.txt"
      cnt=$((cnt + 1))
      tmp=$(mktemp)
    fi
  else
    echo "$line" >> "$tmp"
  fi
done < "$inp"

if [ -s "$tmp" ]; then
  mv "$tmp" "part_$cnt.txt"
fi

rm -f "$tmp"
