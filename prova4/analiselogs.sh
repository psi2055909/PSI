#!/bin/bash

if [[ -z "$1" ]]; then
    echo "Uso: $0 <arquivo_de_configuração>"
    exit 1
fi

config_file="$1"

if [[ ! -f "$config_file" ]]; then
    echo "Arquivo de configuração não encontrado: $config_file"
    exit 1
fi

log_files=()
while IFS= read -r line || [[ -n "$line" ]]; do
    log_files+=("$line")
done < "$config_file"

if [[ ${#log_files[@]} -eq 0 ]]; then
    echo "Nenhum arquivo de log especificado no arquivo de configuração."
    exit 1
fi

tail -f "${log_files[@]}"